/*
 * main.c
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "robcom.h"
#include "outils.h"
#include "regles.h"
#include "ia.h"


int main()
{

  //int sd=-1; //pour communiquer avec l'arbitre
  SDL_Surface *ecran=NULL; //pour afficher le jeu
  //int pions[10][10]; //position
  //int rep;//Entier pour le choix

  //Init graphique
  ecran=init_sdl();
  
  //Init des communications (enlever le commentaire)
  // sd=c_init_socket();


	// TESTS ###########################################################
	Arbre * arbre3 = NULL;
	Arbre * resultat = NULL;
	int useless;

	int joueur = 1;
	
	int i;

	arbre3 = initArbre();

	
	printf("Quel joueur somme nous ? Blanc = 1, noir = -1\nRéponse : ");
	
	scanf("%d", &joueur);
	
	
	for(i = 0; i <= 7 ; i++){
		position_type(arbre3->p.pions, i);

		arbre3->p.pions[4][9] = -2;
		
		arbre3->p.pions[6][1] = -1;
		
		arbre3->p.pions[4][5] = -1;
	
		resultat = creerAnalyse(&arbre3->p, joueur, 3);

		if(resultat != NULL){
			evaluation(resultat, joueur, 0);
			afficheNoeud(resultat, ecran);
			supNoeud(resultat);

			affichePosition(meilleurCoups(&arbre3->p, joueur, 3), ecran);
			scanf("%d", &useless);
			
		}
	}
	
	supNoeud(arbre3);
	// ###############

	quit_sdl();
	
	return 0;
}

