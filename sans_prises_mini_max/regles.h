#ifndef REGLES_H
#define REGLES_H

#include "arbre.h"
	
Arbre * analysePlateau(Position * plateau, int couleur); // Si la couleur peut jouer, renvoie l'arbre des coups possibles, le premier noeud correspond au plateau actuel, 0 sinon

void prisesObligatoires(int i, int j, Arbre * racine, Arbre * noeud_courant, int est_pion);

void testCadran(int i, int j, int sens_i, int sens_j, Arbre * racine, Arbre * noeud_courant, int est_pion);

void testPos(int i, int j, int sens_i, int sens_j, int n, Arbre * racine, Arbre * noeud_courant, int est_pion);



void estDame(int i, int j, Arbre * racine); // Dit si le pion à la position i j devient une dame ou non

int deplacementsPossibles(int i, int j, Arbre * racine);

void testDiagonaleDeplacement(int i, int j, int sens_i, int sens_j, Arbre * racine, int * est_deplace); // Test les deplacements sur la diagonale si le pion est une dame

void testDeplacement(int i, int j, int sens_i, int sens_j, Arbre * racine, int * est_deplace); // Renvoie 1 et ajoute le noeud correspondant à un déplacement à la racine si possible. 0 sinon.
	
#endif
