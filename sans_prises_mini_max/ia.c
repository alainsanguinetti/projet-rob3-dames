#include "ia.h"


// Utilise l'algorithme minimax.

void evaluation(Arbre * noeud, int couleur, int profondeur_actuelle){
	int max = -300000;
	int min = 300000;
	int i = 0;

	//Si on est une feuille on évalue la position à l'aide de la position evalPos
	// on est une feuille donc on a pas de fils
	if(noeud->fils[0] == NULL){
		evalPos(&noeud->p, couleur);

	// Sinon, on évalue les fils et on prend comme valeur le max ou le min de nos fils
	}else{
		while(i < noeud->nb_fils && noeud->fils[i] != NULL){
			evaluation(noeud->fils[i], couleur, profondeur_actuelle + 1);

			if(profondeur_actuelle%2 == 0){ // Max
				if(noeud->fils[i]->p.valeur > max){
					max = noeud->fils[i]->p.valeur;

					noeud->p.valeur = max;
				}
			}else{	// Min
				if(noeud->fils[i]->p.valeur < min){
					min = noeud->fils[i]->p.valeur;

					noeud->p.valeur = min;
				}
			}
			
			i++;
		}
	}
}

void evalPos(Position * p, int couleur){
	int nous = 0;
	int adversaire = 0;
	int delta;

	nous = evalCouleur(p, couleur);
	//adversaire = evalCouleur(p, couleur * -1);

	delta = nous-adversaire;

	p->valeur = delta;
}

int evalCouleur(Position * p, int couleur){
	int i,j;
	int val_pion = 2;
	int val_dame = 10;
	int res=0;

	// On evalue le jeu
	for(i=0; i<10; i++){
		for(j=0;j<10;j++){
			// C'est un pion à nous
			if(couleur * p->pions[i][j] > 0){
				// C'est une dame
				if(couleur * p->pions[i][j]%2 == 0)res+=val_dame;

				// C'est un pion
				else if(couleur * p->pions[i][j]%2 == 1)res+=val_pion;
			}
		}
	}

	return res;
}

Arbre * creerAnalyse(Position * plateau, int couleur, int limite_calcul){
	Arbre * nouveau = NULL;
	Arbre * adversaire = NULL;
	int i = 0;
	
	// On analyse le plateau
	nouveau = analysePlateau(plateau, couleur);

	// Si on a pas perdu
	if(nouveau != NULL){
		// Si on doit encore analyser ou si des pions sont en prises
		if(limite_calcul > 0 || nouveau->nb_prises != 0){

			// Pour chaque coup possible,
			while(i < nouveau->nb_fils && nouveau->fils[i] != NULL){
				// On analyse les coups de l'adversaire
				adversaire = creerAnalyse(&nouveau->fils[i]->p, couleur * -1, limite_calcul -1);

				// Si l'adversaire peut faire qqch
				if(adversaire != NULL){
				// Et on ajoute ses mouvements à notre arbre
					// On fait de la place
					supNoeud(nouveau->fils[i]);

					// Et on ajoute le fils
					nouveau->fils[i] = adversaire;
				}else{
					nouveau->fils[i]->fils[0] = NULL;
				}

				// Et ainsi de suite
				i++;
			}
		}
	}

	return nouveau;
}

Position * meilleurCoups(Position * plateau, int couleur, int limite_calcul){
	Arbre * coups_possibles = NULL;
	Position * meilleur_coups = NULL;
	
	// Creer l'arbre des coups possibles
	coups_possibles = creerAnalyse(plateau, couleur, limite_calcul);

	// Si on n'a pas perdu
	if(coups_possibles != NULL){
		// Evaluer l'arbre
		evaluation(coups_possibles, couleur, 0);

		// Renvoyer le plateau qui correspond au premier fils dont la valeur est celle de la racine
		meilleur_coups = choixCoups(coups_possibles);

		// Nettoyer
		supNoeud(coups_possibles);
	}

	return meilleur_coups;
}

Position * choixCoups(Arbre * coups_possibles){
	int i = 0; // Compteur pour parcourir les fils
	Position * meilleur_coups = NULL;

	i = 0;

	// On parcourt les fils depuis le milieu
		while(coups_possibles->fils[i] != NULL && i < coups_possibles->nb_fils){

			// Si la valeur du fils est égale à la valeur de la racine
			if(coups_possibles->fils[i]->p.valeur == coups_possibles->p.valeur){

				// On crée la Position correspondante
				meilleur_coups = (Position *) malloc(sizeof(Position));

				if(meilleur_coups == NULL){
					printf("choixCoups : erreur d'allocation mémoire\n");
					exit(EXIT_FAILURE);
				}

				copiePosition(&coups_possibles->fils[i]->p, meilleur_coups);

				break;
			}

			i++;
		}

	// On renvoie la Position correspondante
	return meilleur_coups;
}

/*Position * debutDePartie(Position * plateau, int couleur, int coups){
	meilleur_coups = (Position *) malloc(sizeof(Position));

	if(coups == 0){
		copiePosition(plateau, &meilleur_coups->p);

		meilleur_coups->p.pions[



		*/
