#ifndef _OUTILS_
#define _OUTILS_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL.h>
#include <signal.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

/*******************************************************************
 *        FONCTIONS POUR L'AFFICHAGE GRAPHIQUE                     *
 *******************************************************************/

/* Initialisation de l'affichage graphique, à écrire au début de votre main :
   SDL_Surface *ecran=NULL;
   ecran=init_sdl(); */
SDL_Surface * init_sdl();

/*Affichage d'une position donnée à l'écran. Indiquer aussi les noms des opposants
  affiche(pions,ecran,"Jobs","Gates");*/
void affiche (int pions[10][10],SDL_Surface *ecran,char Nom1[],char Nom2[]);

/* Fermeture de l'affichage graphique, à écrire obligatoirement à la fin de votre main :
   quit_sdl();*/
void quit_sdl();

/*******************************************************************
 *                  AUTRES FONCTIONS UTILES                        *
 *******************************************************************/

/* Permet d'initialiser le tableau pions à plusieurs exemples (dont tous ceux
   du site web de la Fédération Française de jeu de dame http://www.ffjd.fr/Web/index.php?page=reglesdujeu)
   l'entier id indique le numéro de l'exemple (id va de 0 à 11)
   Par exemple pour initialiser le tableau pions à la position initiale :
   int pions[10][10];
   position_type(pions,0);*/
void position_type(int pions[10][10], int id);

/* Une petite constante qui permet de passer de l'entier col à la chaine de caractère "BLACK" ou "WHITE"
   printf("%s",SCOL(-1)); affichera "BLACK"
   printf("%s",SCOL(1)); affichera "WHITE"*/
#define SCOL(i) ((i)==-1 ? "BLACK" : "WHITE")

#endif
