#include <stdio.h>
#include <stdlib.h>
#include "outils.h"
#include "regles.h"


// ################# Analyse des coups possibles
Arbre * analysePlateau(Position * plateau, int couleur){
	int deplacement=1;
	Arbre *racine=NULL;
	int i,j;
	int est_pion = 1; // Faux par défaut
	
	racine = creerNoeud(16);
	
	copiePosition(plateau,&racine->p);
	
	racine->nb_prises = 0;
	racine->rafle = 0;
	
	for(i=0;i<10;i++){
		for(j=0;j<10;j++){
			if((racine->p.pions[i][j])*couleur>0){
				
				if(racine->p.pions[i][j] % 2 == 0){
					est_pion = 0;
				}else{
					est_pion = 1;
				}
			
				prisesObligatoires(i, j, racine, racine, est_pion);
				
				// Si on a pas fait de prises
				if(racine->nb_prises == 0){
					deplacement += deplacementsPossibles(i, j, racine);
				}
			}		
		}	
	}
	
	if(racine->nb_prises == 0 && deplacement == 0){
		//    PERDU     //
		printf("analysePlateau : On a perdu !\n");
		/*supNoeud(racine);
		racine = NULL;*/
	}
	
	return racine;
}

void prisesObligatoires(int i, int j, Arbre * racine, Arbre * noeud_courant, int est_pion){
	int sens_i;
	int sens_j;
	
	// Test des prises dans chaque cadran
	// haut gauche
	sens_i=-1;
	sens_j=-1;
	// printf("prisesObligatoires : test du cadran  haut gauche depuis la position [%d, %d]\n", i, j);
	testCadran(i,j,sens_i,sens_j,racine, noeud_courant, est_pion);
	
	// haut droite
	sens_i=1;
	sens_j=-1;
	// printf("prisesObligatoires : test du cadran  haut droit depuis la position [%d, %d]\n", i, j);
	testCadran(i,j,sens_i,sens_j,racine, noeud_courant, est_pion);
	
	// bas droite
	sens_i=1;
	sens_j=1;
	// printf("prisesObligatoires : test du cadran bas droit depuis la position [%d, %d]\n", i, j);
	testCadran(i,j,sens_i,sens_j,racine, noeud_courant, est_pion);
	
	// BAS GAUCHE
	sens_i=-1;
	sens_j=1;
	// printf("prisesObligatoires : test du cadran bas gauche depuis la position [%d, %d]\n", i, j);
	testCadran(i,j,sens_i,sens_j,racine, noeud_courant, est_pion);
}

void testCadran(int i, int j, int sens_i, int sens_j, Arbre * racine, Arbre * noeud_courant, int est_pion){
	int n = 1;
	int i_suivant;
	int j_suivant;
	int i_adversaire;
	int j_adversaire;
	
		// position du pion après la prise d'un pion
		i_suivant = i + (n+1) * sens_i;
		j_suivant = j + (n+1) * sens_j;
		
		i_adversaire = i + n * sens_i;
		j_adversaire = j + n * sens_j;

	// Si on est une dame, on teste sur toute la diagonale jusqu'à trouver un pion ou un mur
	if(est_pion == 0){
		while(i_suivant >= 0 && j_suivant >= 0 && i_adversaire < 9 && j_adversaire  < 9){
			// Si on trouve un pion allié ou un adversaire, on fait et on arrete
			if(noeud_courant->p.pions[i_adversaire][j_adversaire] != 0){
				testPos(i, j, sens_i, sens_j, n, racine, noeud_courant, est_pion);
				break;
			}else{
				n++;
			
				i_suivant = i + (n+1) * sens_i;
				j_suivant = j + (n+1) * sens_j;
				
				i_adversaire = i + n * sens_i;
				j_adversaire = j + n * sens_j;
			}
		}
	// Sinon, on teste une fois
	}else{
		if(i_suivant >= 0 && j_suivant >= 0 && i_adversaire < 9 && j_adversaire  < 9 && noeud_courant->p.pions[i_adversaire][j_adversaire] != 0){
			testPos(i, j, sens_i, sens_j, n, racine, noeud_courant, est_pion);
		}
	}
}

void testPos(int i, int j, int sens_i, int sens_j, int n, Arbre * racine, Arbre * noeud_courant, int est_pion){
	int i_adversaire = i + n * sens_i;
	int j_adversaire = j + n * sens_j;
	int i_suivant;
	int j_suivant;
	int m = 1;
	Arbre * nouveau = NULL;
	
	i_suivant = i_adversaire + m * sens_i;
	j_suivant = j_adversaire + m * sens_j;
	
	// il y a un adversaire à la distance spécifiée	
	if(noeud_courant->p.pions[i_adversaire][j_adversaire] * noeud_courant->p.pions[i][j] < 0){	

		// on teste toutes les positions derrière ce pion
		do{
			// il y a de la place
			if(noeud_courant->p.pions[i_suivant][j_suivant] == 0){
			
				nouveau = creerNoeud(3);
				copiePosition(&noeud_courant->p, &nouveau->p);
				nouveau->rafle = noeud_courant->rafle + 1;
			
				// c'est le nombre de prises que le nouveau noeud doit faire pour être intéressant
				if(noeud_courant->nb_prises > 0){
					nouveau->nb_prises = noeud_courant->nb_prises - 1;
				}else{
					nouveau->nb_prises = 0;
				}
			
				// Déplacements
				nouveau->p.pions[i_adversaire][j_adversaire] = 0;
				nouveau->p.pions[i_suivant][j_suivant] = noeud_courant->p.pions[i][j];
				nouveau->p.pions[i][j] = 0;
			
				/*// Pour le débugage
				SDL_Surface *ecran=init_sdl(); //pour afficher le jeu
				
				afficheNoeud(noeud_courant, ecran);
				
				afficheNoeud(nouveau, ecran);*/
				
			
				// Test de la nouvelle position de notre pion
				prisesObligatoires(i_suivant, j_suivant, racine, nouveau, est_pion);
			
				// Si la position fait suffisament de prise et qu'elle est la dernière de la rafle
				if(nouveau->rafle >= racine->nb_prises){
					
					// Si la position est plus intéressante que toutes celles précédentes, on supprime les anciennes
					if(nouveau->rafle > racine->nb_prises){
						fairePlace(racine);
					}

					// on regarde si on est une dame
					estDame(i_suivant, j_suivant, nouveau);
			
					// on ajoute
					ajoutNoeud(racine, nouveau);
				
					// on met à jour le nombre de prises maximum du joueur
					racine->nb_prises = nouveau->rafle;
				}else{
					supNoeud(nouveau);
				}
			}else{
				break;
			}
			
			m++;
			
			i_suivant = i_adversaire + m * sens_i;
			j_suivant = j_adversaire + m * sens_j;
	
			// printf("testPos : m = %d\n", m);
		}while(i_suivant >= 0 && i_suivant < 10 && j_suivant >= 0 && j_suivant < 10 && est_pion == 0);
	}
}


void estDame(int i, int j, Arbre * noeud){
	int nous = noeud->p.pions[i][j];
	
	// on est noir (-)
	if(nous < 0){
		// on est au bout du plateau
		if(j == 9){
			nous = -2;
			// printf("estDame : je suis devenue une dame noire !!!!\n");
		}
	}else{
	//on est blanc
		// on est au bout du plateau
		if(j == 0){
			nous = 2;
			// printf("estDame : je suis devenue une dame blanche !!!!\n");
		}
	}
			
	noeud->p.pions[i][j] = nous;
}


// ################# Analyse des déplacements possibles
int deplacementsPossibles(int i, int j, Arbre * racine){
	int nous = racine->p.pions[i][j];
	int sens_i;
	int sens_j;
	int est_deplace = 0;
	
	// printf("deplacementsPossibles : analyse des déplacements possibles depuis la position [%d ; %d]\n",i,j);
	
	if((nous * nous) == 1){ // On est un pion : 2 deplacements possibles
		sens_j = -1 * nous;
		
		// A gauche
		sens_i = 1;
		testDeplacement(i, j, sens_i, sens_j, racine, &est_deplace);
		
		// A droite
		sens_i = -1;
		testDeplacement(i, j, sens_i, sens_j, racine, &est_deplace);
		
		
	}else{ // On est une dame :)
		// 4 directions possibles
		// Vers le haut
		sens_j = -1;
			// A gauche
			sens_i = -1;
			testDiagonaleDeplacement(i, j, sens_i, sens_j, racine, &est_deplace);
			
			// A droite
			sens_i = 1;
			testDiagonaleDeplacement(i, j, sens_i, sens_j, racine, &est_deplace);

		// Vers le bas
		sens_j = 1;
			// A gauche
			sens_i = -1;
			testDiagonaleDeplacement(i, j, sens_i, sens_j, racine, &est_deplace);
			
			// A droite
			sens_i = 1;
			testDiagonaleDeplacement(i, j, sens_i, sens_j, racine, &est_deplace);
	}	
	
	return est_deplace;
}

void testDiagonaleDeplacement(int i, int j, int sens_i, int sens_j, Arbre * racine, int * est_deplace){
	int i_local = i;
	int j_local = j;
	int n = 1;
	
	while(i_local >= 0 && i_local < 10 && j_local >= 0 && j_local < 10){
		testDeplacement(i, j, n * sens_i, n * sens_j, racine, est_deplace);
				
		n++;
				
		i_local+=sens_i;
		j_local+=sens_j;

		if(i_local < 0 || i_local >= 10 || j_local < 0 || j_local > 10 || racine->p.pions[i_local][j_local] != 0){
			break;
		}
		
	}
}

void testDeplacement(int i, int j, int sens_i, int sens_j, Arbre * racine, int* est_deplace){
	int i_suivant = i + sens_i;
	int j_suivant = j + sens_j;
	int nous = racine->p.pions[i][j];
	Arbre * noeud = NULL;
	
	if(i_suivant >= 0 && i_suivant < 10 && j_suivant >= 0 && j_suivant < 10 && racine->p.pions[i_suivant][j_suivant] == 0){

		noeud = creerNoeud(1);
	
		copiePosition(&racine->p, &noeud->p);
	
		noeud->p.pions[i_suivant][j_suivant] = nous;
		noeud->p.pions[i][j] = 0;
	
		ajoutNoeud(racine, noeud);
		
		estDame(i_suivant, j_suivant, noeud);
		
		*est_deplace = 1;
	}
}


