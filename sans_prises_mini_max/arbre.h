#ifndef ARBRE_H
#define ARBRE_H

#include <stdio.h>
#include <stdlib.h>
#include "outils.h"

// Notre structure
typedef struct une_position {
	// le plateau actuel
	int pions[10][10];
	int valeur;
	}
Position;

typedef struct un_arbre Arbre;
struct un_arbre {
	Position p;
	int nb_prises;
	int rafle;
	int nb_fils;
	Arbre** fils;
};

typedef struct prises_dames{
	int nb;
	int ** pions;
} Prises;

// Nos fonctions

// Créer un arbre
Arbre * initArbre();

// Créer un noeud
Arbre * creerNoeud(int nb_de_fils);

// Supprimer un noeud
void supNoeud(Arbre * noeud);

// Faire de la place dans les fils d'un noeud : supprimer tous ces fils
void fairePlace(Arbre * noeud);

// Afficher le plateau d'une position donnée
void affichePosition(Position * p, SDL_Surface * ecran);

// Afficher un noeud
void afficheNoeud(Arbre * noeud, SDL_Surface * ecran);

// Ajouter un noeud
void ajoutNoeud(Arbre * pere, Arbre * fils);

// Copier un plateau
void copiePosition(Position * source, Position * destination);

// Initialiser le tableau de prises
Prises * initPrise(int l);

// Supprimer un tableau de prises
void supPrise(Prises * prises);

// Ajouter une prise au tableau de prises
void ajoutPrise(Prises * prises, int i, int j);

// Tester si une prise a déjà été faite
int testPrises(Prises * prises, int i, int j);


#endif
