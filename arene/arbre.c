#include <stdio.h>
#include <stdlib.h>
#include "outils.h"
#include "arbre.h"



Arbre * initArbre(){
	//printf("Création d'un arbre\n");
	Arbre * racine = NULL;
	
	// Creer un noeud
	racine = creerNoeud(9);
	
	if (racine == NULL){
		printf("\tL'arbre n'a pas été créé\n");
	}
	
	//printf("\tArbre créé\n");
	
	// Renvoyer l'adresse de la racine de l'arbre
	return racine;
}

Arbre * creerNoeud(int nb_de_fils){
	//printf("Je vais essayer de creer un noeud\n");
	Arbre * nouveau = NULL;
	int i = 0;
	
	// Allocation mémoire
	nouveau = (Arbre *)malloc(sizeof(Arbre));
	if(nouveau == NULL){
		printf("creerNoeud : Ya une couille !\n");
		exit(EXIT_FAILURE);
	}
	
	//Initialisation des paramètres
	nouveau->nb_prises = 0;
	nouveau->nb_fils = nb_de_fils;

	//printf("\tAffiliation de %d fils\n", nouveau->nb_fils);
		
	// Si il n'y a pas de fils demandé, on initialise le tableau quand même
	if(nouveau->nb_fils == 0){
		nb_de_fils = 1;
		printf("creerNoeud : pas de fils\n");
	}
	
	// Creation du tableau de pointeur vers les fils
	nouveau->fils = (Arbre**)malloc(nb_de_fils * sizeof(Arbre*));

	if(nouveau->fils == NULL){
		printf("creerNoeud : Ya une couille !\n");
		exit(EXIT_FAILURE);
	}else{
		// Initialisation du tableau à NULL
		for(i = 0; i < nouveau->nb_fils; i++){
			nouveau->fils[i] = NULL;
		}
	}

	position_type(nouveau->p.pions, 0);
	nouveau->p.valeur = -1000; // Important pour le fonctionnement de l'algorithme NegaMax
	
	return nouveau;
}

void supNoeud(Arbre * noeud){		// Cette fonction permet de supprimer "noeud" et ses fils. Elle permet donc de supprimer un arbre entier
	//printf("Suppression d'un noeud\n");
	
	int i = 0;
	if(noeud != NULL){
	//printf("\tCe noeud a maximum %d fils\n", noeud->nb_fils);
	
		// Si pas de fils, libération du noeud
		if(noeud->nb_fils == 0){
			free(noeud->fils);
			//printf("\tPas de fils, suppression du noeud.\n");
			free(noeud);
		}else{
			// Libération des fils
			//printf("\tSuppression des fils\n");
		
			while(i < noeud->nb_fils && noeud->fils[i] != NULL){
				//printf("\t\tSuppression du fils %d\n", i + 1);
				supNoeud(noeud->fils[i]);
				i++;
			}

			free(noeud->fils);
			//printf("\tPas de fils, suppression du noeud.\n");
			free(noeud);
		}
	// Fin
	}
}

void affichePosition(Position * p, SDL_Surface * ecran){
	printf("\tCette position est évaluée à %d\n", p->valeur);
	affiche(p->pions,ecran,"Albert","Louis");
}

void afficheNoeud(Arbre * noeud, SDL_Surface * ecran){
	if(noeud == NULL){
		printf("Affichage du noeud impossible : il n'existe pas.\n");
	}else{
	
		printf("Affichage d'un noeud\n");
		int fiston = -1;
	
		do{
			// Affichage de la position correspondante
			affichePosition(&noeud->p, ecran);
	
			printf("A partir de ce noeud : %d prises\n", noeud->nb_prises);
	
			if(noeud->nb_fils == 0){
				printf("\tCe noeud n'a pas de fils\n");
		
				printf("\tEntrez un nombre négatif pour sortir de l'affichage de ce noeud : ");
				scanf("%d", &fiston);
		
			}else{
				// Présentation des fils
				printf("\tCe noeud a maximum %d fils.\n", noeud->nb_fils);
	
				// Choix du fils à afficher
				printf("\tEntrez le numéro du fils à afficher (nb négatif pour sortir) : ");
				scanf("%d", &fiston);
		
				// Sortie
				if(fiston < 0){
					printf("Sortie : retour au noeud supérieur.\n");
		
				// Ou affichage du fils
				}else if(fiston < noeud->nb_fils && noeud->fils[fiston] != NULL){
					printf("\tAffichage du fils %d\n", fiston);
					afficheNoeud(noeud->fils[fiston],ecran);
				}
			}
		// Faut-il réafficher ce noeud ?
		}while(fiston >= 0);
	}
}

void ajoutNoeud(Arbre * pere, Arbre * fils){
	int i = 0;
	Arbre ** fils_realloc = NULL;
	int place = 0;
	
	if(fils != NULL && pere != NULL){
			
		// Vérification de la place pour le nouveau fils sur le noeud pere
		// printf("Vérification de la place sur le noeud pere\n");
	
		// Si il n'y a pas de fils il n'y a pas de place
		if(pere->nb_fils == 0){
			// printf("\tPas de fils donc pas de place pour l'instant\n");
			place = -1;

		// Si il y a des fils, on regarde quelle place est libre
		}else{
			// Si toutes les cases du tableau sont occupées il n'y a pas de place
			while(place < pere->nb_fils && pere->fils[place] != NULL){
				//printf("\tPlace %d occupée\n", place);
				i++;
				place++;
			};

			if(place == pere->nb_fils){
				//printf("\tToutes les cases sont occupées donc pas de place pour l'instant\n");
				place = -1;
			}
		}
		
		
		// S'il n'y a pas de place, ajout d'une nouvelle place
		if(place < 0){
			//printf("\tCréation d'une nouvelle place\n");
			fils_realloc = realloc(pere->fils, (pere->nb_fils + 1) * sizeof(Arbre *));
		
			if(fils_realloc == NULL){
				printf("ajoutNoeud : problème de réallocation\n");
				exit(EXIT_FAILURE);
			}else{
				//printf("\tPlace créée\n");
				pere->fils = fils_realloc;
				place = i;
				pere->nb_fils++;
			}
		}
	
		// Ajout du fils à la dernière place libre
		// printf("\tAjout d'un fils à la place %d\n", place);
		pere->fils[place] = fils;
	}
}

void copiePosition(Position * source, Position * destination){
	int i, j;
	
	//printf("Copie d'une 'Position'\n");
	
	for(j = 0; j < 10; j++){
		for(i = 0; i < 10; i++){
			destination->pions[i][j] = source->pions[i][j];
		}
	}
	
	destination->valeur = source->valeur;
}

void fairePlace(Arbre * noeud){
	int i = 0;
	
	while(i < noeud->nb_fils && noeud->fils[i] != NULL){
		supNoeud(noeud->fils[i]);
		noeud->fils[i] = NULL;
		i++;
	}
}

Prises * initPrise(int l){
	int i = 0;
	Prises * nouvelle = NULL;
	
	nouvelle = (Prises *)malloc(sizeof(Prises));
	
	if(nouvelle != NULL){
		nouvelle->pions = (int **)malloc(2 * sizeof(int*));
		
		nouvelle->pions[0] = (int *)malloc(l * sizeof(int));
		
		nouvelle->pions[1] = (int *)malloc(l * sizeof(int));
		
		if(nouvelle->pions != NULL && nouvelle->pions[0] != NULL){
			for(i = 0; i < l; i++){
				nouvelle->pions[0][i] = 10;
			}
			
			nouvelle->nb = l;
		}
		
	}else{
		printf("initPrise : tableau des prises non initialisé\n");
	}
	
return nouvelle;
}

void supPrise(Prises * prises){
	free(prises->pions[0]);
	free(prises->pions[1]);
	free(prises->pions);
	free(prises);
}

void ajoutPrise(Prises * prises, int i, int j){
	if(prises != NULL && prises->pions != NULL){
		int m = 0;
		
		while(prises->pions[0][m] != 10 && m < prises->nb){
			m++;
		}
		
		prises->pions[0][m] = i;
		prises->pions[1][m] = j;
	}else{
		printf("ajoutPrise : ajout imposssible !\n");
	}
}

int testPrises(Prises * prises, int i, int j){
	int m = 0;
	int test = 2;
	
	
	if(prises != NULL){
	
	// printf("testPrises : test [%d, %d]\n",i,j);
	
		if(i < 10 && i >= 0 && j < 10 && j >= 0){
			for(m=0;m < prises->nb; m++){
				if(prises->pions[0][m] == i){
					if(prises->pions[1][m] == j){
						// printf("testPrises : c'était la place d'un pion ennemi\n");
						test = 0;
					}
				}
			}
		}else{
			// printf("testPrises : hors plateau\n");
			test = 0;
		}
	}else{
		printf("testPrises : prises non définie\n");
	}
	
	return test;
}
