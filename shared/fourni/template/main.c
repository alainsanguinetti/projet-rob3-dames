#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <robcom.h>
#include <outils.h>

int main() {
  /* INITIALISATION DU PROGRAMME */

  int sd=-1; //pour communiquer avec l'arbitre
  SDL_Surface *ecran=NULL; //pour afficher le jeu
  int pions[10][10]; //position
  int rep;//Entier pour le choix

  //Init graphique
  ecran=init_sdl();
  
  //Init des communications (enlever le commentaire)
  // sd=c_init_socket();

  /* ECRIRE ICI LE PROGRAMME PRINCIPAL */
  
  //initialisation la position des pions
  position_type(pions,11);

  //Affichage de la position initiale des pions
  affiche(pions,ecran,"Paul","John");

  //Donner un choix pour que le programme s'interrompt
  printf("Entrer un nombre pour quitter le programme\n");
  scanf("%d",&rep);
    

  /* FERMETURE DU PROGRAMME */

  //Fermeture de l'affichage
  quit_sdl();
  
  //Fermeture des communications (enlever le commentaire)
  //c_clean_socket(sd,-1,-1);
  return(0);
  
  //Astuce pour éviter le warning "unused variable"
  sd--;
}
