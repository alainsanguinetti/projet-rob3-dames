#ifndef IA_H
	#define IA_H

	#include <stdlib.h>
	#include <time.h>
	
	#include "arbre.h"
	
	#include "regles.h"
	
	void evalPos(Position * p, int couleur, int robslayer);	// Evalue un plateau

	int evalCouleur(Position * p, int couleur);	// Evalue une des couleurs

	int evalLignes(Position * p, int couleur); // Evalue une des couleurs en valorisant les lignes du fond

	int evalTemps(Position * p, int couleur);	// Compte les temps

	void evaluation(Arbre * noeud, int couleur, int profondeur_actuelle);	// Evalue un arbre

	Arbre * creerAnalyse(Position * plateau, int couleur, int limite_calcul, int alpha, int beta, int joueur, int robslayer);	// Creer un arbre des coups jouables alternativement par les deux joueurs

	Position * meilleurCoups(Position * plateau, int couleur, int limite_calcul, int robslayer);	// Retourne le plateau qui est le meilleur coup à jouer à partir du plateau pion donné en arguments

	Position * choixCoups(Arbre * coups_possibles); 	// Retourne un plateau correspondant à la valeur de la racine

	Position * debutDePartie(Position * plateau, int couleur, int coups);
#endif
