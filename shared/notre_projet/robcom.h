#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/un.h>
#include <unistd.h>

#ifndef _ROBCOM_
#define _ROBCOM_

/* Chemin des sockets par défaut */
#define SERVER_PATH     "/tmp/server"

/* Taille du tableau commiqué au serveur (10x10=100) */
#define BUFFER_LENGTH    100
#define FALSE              0

/* Initialisation de la connection, à écrire au début de votre main :
int sd;
sd=c_init_socket(); */
int  c_init_socket  () ;

/* Envoi du nom à l'arbitre :
   c_envoie_nom("Robslayer",sd); */
void c_envoie_nom(char Nom[],int sd2);

/* Récupération de votre couleur de jeu -1 pour noir, 1 pour blanc :
int col;
col=c_receive_col(sd); */
int  c_receive_col(int sd2);

/* Récupération de la position envoyé par l'arbitre, cette position sera forcément valide et vous sert de point de départ pour jouer un coup :
int pions[10][10];
c_receive_pos(pions,sd);*/
void c_receive_pos(int pions[10][10],int sdx);

/* Envoi de la position que vous souhaitez jouer. Cette position devra être valide. Si pions[0][0]=5 et pions[0][1]=col, vous enverrez à l'arbitre l'information que le joueur col a gagné, mais normalement, ça ne devrait pas se produire.*/
void c_send_pos(int pions[10][10],int sdx);

/* Fermeture de la connection,à écrire obligatoirement à la fin de votre main :
c_clean_socket(sd,-1,-1);*/
void c_clean_socket (int sd1,int sd2, int sd3);

#endif
