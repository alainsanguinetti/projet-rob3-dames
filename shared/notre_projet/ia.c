#include "ia.h"


void evalPos(Position * p, int couleur, int robslayer){
	int nous = 0;
	int adversaire = 0;
	int delta;

	if(robslayer == 0){
		// Nb de pions
		nous = evalCouleur(p, couleur);
		adversaire = evalCouleur(p, couleur * -1);

		delta = nous-adversaire;

		p->valeur+=(2 * delta);

		// Proximité du haut
		nous = evalLignes(p, couleur);
		adversaire = evalLignes(p, couleur * -1);

		delta = nous-adversaire;

		p->valeur+= delta;

		// Temps
		nous = evalLignes(p, couleur);
		adversaire = evalLignes(p, couleur * -1);

		delta = adversaire-nous;

		p->valeur+= delta;

	// La version spéciale robslayer
	}else{
		// Nb de pions
		nous = evalCouleur(p, couleur);
		adversaire = evalCouleur(p, couleur * -1);

		delta = nous-adversaire;

		p->valeur+= delta;

		// Proximité du haut
		nous = evalLignes(p, couleur);
		adversaire = evalLignes(p, couleur * -1);

		delta = nous-adversaire;

		p->valeur+= delta;
	}
}

int evalCouleur(Position * p, int couleur){
	int i,j;
	int val_pion = 1;
	int val_dame = 4;
	int res=0;
	int nb_pions = 0;
	int nb_dames = 0;

	// On evalue le jeu, on compte les pions
	for(i=0; i<10; i++){
		for(j=0;j<10;j++){
			// C'est un pion à nous
			if(couleur * p->pions[i][j] > 0){
				// C'est une dame
				if(couleur * p->pions[i][j]%2 == 0)nb_dames++;

				// C'est un pion
				else if(couleur * p->pions[i][j]%2 == 1)nb_pions++;
			}
		}
	}

	res+=(nb_pions * val_pion);

	if(nb_pions < 10){
		val_dame+= 2;
	}

	res+=(nb_dames * val_dame);

	return res;
}

int evalLignes(Position * p, int couleur){
	int i, j; // i colonnes, j lignes
	int val_pion = 1;
	int res=0;

	if(couleur == -1){ // On est noir
		for(i = 0; i < 10; i ++){
			for(j = 5; j < 9; j ++){
				// Un pion à nous
				if(couleur * p->pions[i][j] > 0){
					// C'est un pion
					if(couleur * p->pions[i][j]%2 == 1)res+=val_pion * (j/2);
				}
			}
		}
	}else{
		for(i = 0; i < 10; i ++){
			for(j = 0; j < 4; j ++){
				// Un pion à nous
				if(couleur * p->pions[i][j] > 0){
					// C'est un pion
					if(couleur * p->pions[i][j]%2 == 1)res+=val_pion * (9 - j)/2;
				}
			}
		}
	}

	return res;
}

int evalTemps(Position * p, int couleur){
	int i, j;
	int res = 0;

	if(couleur == -1){
		for(i = 0; i < 10; i++){
			for(j = 0; j <10; j++){
				if(couleur * p->pions[i][j] > 0){
					res+=j;
				}
			}
		}
	}else{
		for(i = 0; i < 10; i++){
			for(j = 0; j <10; j++){
				if(couleur * p->pions[i][j] > 0){
					res+=(10 - j);
				}
			}
		}
	}

	return res;
}


		
// Utilise l'algorithme NegaMax avec l'élagage

Arbre * creerAnalyse(Position * plateau, int couleur, int limite_calcul, int alpha, int beta, int joueur, int robslayer){
	Arbre * nouveau = NULL;
	Arbre * adversaire = NULL;
	int i = 0;
	int val = 0;
	int meilleur = -4000;
	
	// On analyse le plateau
	nouveau = analysePlateau(plateau, couleur);

	// Si on a pas perdu
	if(nouveau != NULL){
		// Si on doit encore analyser ou si des pions sont en prises
		if(limite_calcul > 0 || nouveau->nb_prises != 0){

			meilleur = - 4000;

			// Pour chaque coup possible,
			while(i < nouveau->nb_fils && nouveau->fils[i] != NULL){

				// On analyse les coups de l'adversaire
				adversaire = creerAnalyse(&nouveau->fils[i]->p, couleur * -1, limite_calcul -1, -beta, -alpha, joueur, robslayer);
				
				// Si l'adversaire peut faire qqch
				if(adversaire != NULL){

					val = -adversaire->p.valeur;

					// Max
					if(val > meilleur){
						meilleur = val;

						 if(meilleur > alpha){
							alpha = meilleur;

							if(alpha >= beta){ // COUPURE !!
								nouveau->p.valeur = meilleur;

								supNoeud(adversaire);

								break;
							}
						}
					}
					
					// Et on ajoute ses mouvements à notre arbre
					// On fait de la place
					supNoeud(nouveau->fils[i]);

					// Et on ajoute le fils
					nouveau->fils[i] = adversaire;

				// Si l'adversaire ne fait rien
				}else{
					nouveau->fils[i]->fils[0] = NULL;
				}

				// Et ainsi de suite
				i++;
			}

			nouveau->p.valeur = meilleur;

		// On est une feuille
		}else{
			evalPos(&nouveau->p, couleur, robslayer);
		}
	}

	return nouveau;
}

Position * meilleurCoups(Position * plateau, int couleur, int limite_calcul, int robslayer){
	Arbre * coups_possibles = NULL;
	Position * meilleur_coups = NULL;
	
	// Creer l'arbre des coups possibles
	coups_possibles = creerAnalyse(plateau, couleur, limite_calcul, -30000, 300000, couleur, robslayer);

	// Si on n'a pas perdu
	if(coups_possibles != NULL){
		// Renvoyer le plateau qui correspond au premier fils dont la valeur est celle de la racine
		meilleur_coups = choixCoups(coups_possibles);

		// Nettoyer
		supNoeud(coups_possibles);
	}

	return meilleur_coups;
}

Position * choixCoups(Arbre * coups_possibles){
	int i = 0; // Compteur pour parcourir les fils
	Position * meilleur_coups = NULL;

	i = 0;

	// On parcourt les fils depuis le milieu
		while(coups_possibles->fils[i] != NULL && i < coups_possibles->nb_fils){

			// Si la valeur du fils est égale à la valeur de la racine
			if(coups_possibles->fils[i]->p.valeur == -coups_possibles->p.valeur){

				// On crée la Position correspondante
				meilleur_coups = (Position *) malloc(sizeof(Position));

				if(meilleur_coups == NULL){
					printf("choixCoups : erreur d'allocation mémoire\n");
					exit(EXIT_FAILURE);
				}

				copiePosition(&coups_possibles->fils[i]->p, meilleur_coups);

				meilleur_coups->valeur = coups_possibles->p.valeur;

				break;
			}

			i++;
		}

	// On renvoie la Position correspondante
	return meilleur_coups;
}
