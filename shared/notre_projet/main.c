/*
 * main.c
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#define NOM "RobKisser"
#define PROFONDEUR 4


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "robcom.h"
#include "outils.h"
#include "regles.h"
#include "ia.h"

int main()
{
// Initialisation
	int couleur = 0;
	Position * coups = NULL;
	Position * plateau = (Position *)malloc(sizeof(Position));
	int sd=-1; //pour communiquer avec l'arbitre
	int limite_calcul = PROFONDEUR;
	int tour = 0;
	int robslayer = 0;
/*
// Affichage
	int useless = 0;
	SDL_Surface * ecran = init_sdl();*/
	
// Connexion au programme arbitre
	//Init des communications (enlever le commentaire)
	sd=c_init_socket();

// Contre robslayer ?
	printf("Est-ce que je joue contre robslayer ?\n0 = non, 1 = oui");
	scanf("%d", &robslayer);

// Choix de la profondeur de calcul
	printf("Quelle profondeur de calcul souhaitez-vous ?\nRéponse :");
	scanf("%d", &limite_calcul);

	printf("Je vais jouer avec une profondeur de calcul de %d\n", limite_calcul);



// Authentification
	c_envoie_nom(NOM, sd);

// Affectation de notre couleur
	couleur = c_receive_col(sd);
	printf("Couleur recue : %d\n", couleur);

// JEU
	do{
		printf("Coups %d\n", tour);

		printf("\t1 : Réception du plateau\n");
		c_receive_pos(plateau->pions, sd);

		// Si le jeu est fini
		if(plateau->pions[0][0] == 5){
			if(plateau->pions[0][1] == 10){
				printf("Match nul.\n");
				break;
			}else if(plateau->pions[0][1] == couleur){
				printf("On a gagné !\n");
				break;
			}else if (plateau->pions[0][1] == -1 * couleur){
				printf("On a perdu ..\n");
				break;
			}
			
		// Tant qu'on a pas gagné ou perdu
		}else{
			coups = meilleurCoups(plateau, couleur, limite_calcul, robslayer);

			if(coups != NULL){
				printf("\t2 : Choix du coup que nous allons jouer\n");
				printf("\t3 : Envoi de notre coup, évalué à %d\n", coups->valeur);

				/*
				// DÉBUGAGE
				affichePosition(coups, ecran);
				scanf("%d", &useless);
				*/

				c_send_pos(coups->pions, sd);

				free(coups);
			}else{
				plateau->pions[0][0] = 5;
				plateau->pions[0][1] = -1 * couleur;
			}
		}

		tour++;
	}while(1);

// Fin
	free(plateau);
	c_clean_socket(sd,-1,-1);
	quit_sdl();
	
	return 0;
}

